import 'package:flutter/material.dart';


class Home extends StatefulWidget {
  var name, num, est, loc;

  Home({this.name, this.num, this.est, this.loc});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
body: Center(
  child: Text('${widget.name}'),
),
    );
  }
}
