import 'package:bars/pages/Home.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class addRest extends StatefulWidget {
  @override
  _addRestState createState() => _addRestState();
}

// ignore: camel_case_types
class _addRestState extends State<addRest> {
  var restaurant;
  var number;
  var date;
  var location;

  final _restaurantCon = new TextEditingController();
  final _numberCon = new TextEditingController();
  final _dateCon = new TextEditingController();
  final _locationCon = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Add Restaurants'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(
            left: 15.0, right: 15.0, top: 20.0, bottom: 10.0),
        child: Card(
          elevation: 10.0,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Your Restaurant Details',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Expanded(
                  child: Form(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 20.0),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _restaurantCon,
                            keyboardType: TextInputType.name,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(top: 2),
                              labelText: 'Name of the Restaurant',

                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: TextFormField(
                              controller: _numberCon,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(top: 2),
                                labelText: 'Phone Number',
                                hintText: '10 Characters',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: TextFormField(
                              controller: _dateCon,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(top: 2),
                                labelText: 'Date of Establishment',
                                hintText: 'DD/ MM/ YYYY',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: TextFormField(
                              controller: _locationCon,
                              keyboardType: TextInputType.streetAddress,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(top: 2),
                                labelText: 'Location',
                              ),
                            ),
                          ),
                          Spacer(),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 30.0, bottom: 30.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: RaisedButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Home(
                                                  name: _restaurantCon,
                                                  num: _numberCon,
                                                  est: _dateCon,
                                                  loc: _locationCon,
                                                )));
                                  },
                                  child: Text('Submit'),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
